import tkinter as tk
import time
import random






class Application:

    def __init__(self, master):

        self.master = master
        self.score = 0
        self.best_time = float("inf")
        self.previous_time = 0

        self.canvas = tk.Canvas(self.master, width=400, height=400)
        self.canvas.pack()

        self.score_label = tk.Label(self.master, text="Score : 0", fg="red", font=("Arial", 20))
        self.score_label.pack(anchor="n")

        self.time_label = tk.Label(self.master, text="Best Time : N/A", fg="red", font=("Arial", 20))
        self.time_label.pack(anchor="n")

        self.diff_label = tk.Label(self.master, text="Time Difference : N/A", fg="black", font=("Arial", 20))
        self.diff_label.pack(anchor="n")

        self.start_button = tk.Button(self.master, text="Start Game", command=self.start_game, font=("Arial", 20))
        self.start_button.pack(anchor="n")

        self.start_time = None

        self.canvas.bind("<Button-1>", self.click_circle)

    def start_game(self):

        self.start_button.config(state=tk.DISABLED)

        self.create_circle()
        self.start_time = time.time()

    def create_circle(self):
        
        self.canvas.delete("circle")

        x = random.randint(50, 350)
        y = random.randint(50, 350)

        self.circle = self.canvas.create_oval(x-25, y-25, x+25, y+25, fill="blue", tags="circle")

    def click_circle(self, event):

        item = self.canvas.find_withtag("current")

        if item and item[0] == self.circle:

            self.score += 1
            self.score_label.config(text=f"Score : {self.score}", fg="black")

            elapsed_time = time.time() - self.start_time
            self.start_time = time.time()

            if self.previous_time is not None:

                time_difference = (self.start_time - self.previous_time)
                
                if time_difference < elapsed_time:

                    self.time_label.config(text="Best Time : {:.2f} seconds".format(self.best_time), fg="black")

                    self.diff_label.config(text="Time Difference : {:.2f} seconds".format(time_difference), fg="green")

                else:

                    self.diff_label.config(text="Time Difference : {:.2f} seconds".format(time_difference))
                    self.diff_label.config(fg="red")


            if elapsed_time < self.best_time:

                self.best_time = elapsed_time
                self.time_label.config(text="Best Time : {:.2f} seconds".format(self.best_time), fg="black")

            self.previous_time = self.start_time
            self.create_circle()

                
        #self.start_button.config(state=tk.NORMAL)




root = tk.Tk()
game = Application(root)
root.mainloop()
